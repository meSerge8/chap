package host

import (
	"bytes"
	"chap_lab/chap"
	"errors"
	"math/rand"
	"net"
	"time"
)

func (h *Host) Auth(conn net.Conn) (name string, err error) {
	peerName, err := readUsername(conn)
	if err != nil {
		return "", err
	}

	err = h.checkUsername(string(peerName))
	if err != nil {
		return "", errors.New("Unknown user")
	}

	id := chap.ToBytes(h.getId())
	challenge, hash := h.challenge(id, peerName)

	err = chap.WriteBytes(conn, challenge)
	if err != nil {
		return "", err
	}

	solved, err := readSolved(conn)
	if err != nil {
		return "", err
	}

	b := check(solved, id, hash, peerName)

	var resp []byte
	var authErr error
	if b {
		resp = success(id)
	} else {
		resp = fail(id)
		authErr = errors.New("auth not passed")
	}

	if err = chap.WriteBytes(conn, resp); err != nil {
		return "", err
	}

	return string(peerName), authErr
}

func (h *Host) checkUsername(x string) error {
	if _, ok := h.Database[x]; ok {
		return nil
	}
	return errors.New("Unknown user")
}

func (h *Host) challenge(id, peer []byte) (challenge, checkHash []byte) {
	rand.Seed(time.Now().UnixNano())
	random := chap.ToBytes(rand.Uint32())

	resp := [][]byte{
		{byte(1)},
		id,
		random,
		h.myName}
	challenge = bytes.Join(resp, []byte{})

	checkHash = chap.Hash(id, random, []byte(h.Database[string(peer)]))

	return
}

func check(solved, id, hash, peerName []byte) bool {
	if len(solved) < 22 {
		return false
	}
	if solved[0] != byte(2) {
		return false
	}

	isEqual := func(x, y []byte) bool {
		return bytes.Compare(x, y) == 0
	}

	if !isEqual(solved[1:5], id) {
		return false
	}
	if !isEqual(solved[5:21], hash) {
		return false
	}
	if !isEqual(solved[21:], peerName) {
		return false
	}

	return true
}

func success(id []byte) []byte {
	resp := [][]byte{
		{byte(3)},
		id,
		[]byte("Welcome to the club, buddy")}

	return bytes.Join(resp, []byte{})
}

func fail(id []byte) []byte {
	resp := [][]byte{
		{byte(4)},
		id,
		[]byte("Oh ... Im sorry")}

	return bytes.Join(resp, []byte{})
}
