package host

import (
	"sync"
)

type Host struct {
	Database map[string]string
	myName   []byte
	id       uint32
	mut      sync.Mutex
}

func NewHost(myName string) Host {
	return Host{
		Database: make(map[string]string),
		myName:   []byte(myName),
		id:       0,
		mut:      sync.Mutex{}}
}

func (h *Host) getId() uint32 {
	h.mut.Lock()
	defer h.mut.Unlock()
	h.id++
	return h.id
}
