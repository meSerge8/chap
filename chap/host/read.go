package host

import (
	"bufio"
	"errors"
	"net"
)

func readUsername(conn net.Conn) ([]byte, error) {
	r := bufio.NewReader(conn)
	bs, err := r.ReadBytes(0)
	return bs[:len(bs)-1], err
}

func readSolved(conn net.Conn) ([]byte, error) {
	r := bufio.NewReader(conn)
	res := make([]byte, 21)

	n, err := r.Read(res)
	if err != nil {
		return nil, err
	}
	if n != 21 {
		return nil, errors.New("Expected: n = 21")
	}

	bs, err := r.ReadBytes(0)
	if err != nil {
		return nil, err
	}

	res = append(res, bs...)

	return res[:len(res)-1], nil
}
