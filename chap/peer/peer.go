package peer

type peer struct {
	Database map[string]string
	myName   []byte
}

func NewPeer(myName string) peer {
	return peer{
		Database: make(map[string]string),
		myName:   []byte(myName)}
}
