package peer

import (
	"bufio"
	"errors"
	"net"
)

func readChallenge(conn net.Conn) ([]byte, error) {
	r := bufio.NewReader(conn)
	res := make([]byte, 9)

	n, err := r.Read(res)
	if err != nil {
		return nil, err
	}
	if n != 9 {
		return nil, errors.New("Expected: n = 9")
	}

	bs, err := r.ReadBytes(0)
	if err != nil {
		return nil, err
	}

	res = append(res, bs...)

	return res[:len(res)-1], nil
}

func readCheck(conn net.Conn) ([]byte, error) {
	r := bufio.NewReader(conn)
	res := make([]byte, 5)

	n, err := r.Read(res)
	if err != nil {
		return nil, err
	}
	if n != 5 {
		return nil, errors.New("Expected: n = 5")
	}

	bs, err := r.ReadBytes(0)
	if err != nil {
		return nil, err
	}

	res = append(res, bs...)

	return res[:len(res)-1], nil
}
