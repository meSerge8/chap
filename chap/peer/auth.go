package peer

import (
	"bytes"
	"chap_lab/chap"
	"errors"
	"net"
)

func (p *peer) Auth(conn net.Conn) (bool, error) {
	chap.WriteBytes(conn, p.myName)

	challenge, err := readChallenge(conn)
	if err != nil {
		return false, err
	}

	solved, err := p.solveChallenge(challenge)
	if err != nil {
		return false, err
	}

	err = chap.WriteBytes(conn, solved)
	if err != nil {
		return false, err
	}

	bs, err := readCheck(conn)
	if err != nil {
		return false, err
	}

	return check(bs)
}

func check(bs []byte) (result bool, err error) {
	switch bs[0] {
	case 3:
		result = true
	case 4:
		result = false
	default:
		err = errors.New("Invalid status code")
	}

	return result, err
}

func (p *peer) solveChallenge(challenge []byte) ([]byte, error) {
	if challenge[0] != 1 {
		return nil, errors.New("wrong format")
	}
	if len(challenge) < 10 {
		return nil, errors.New("wrong format")
	}

	id := challenge[1:5]
	random := challenge[5:9]
	hostName := challenge[9:]
	passwd := []byte(p.Database[string(hostName)])
	hash := chap.Hash(id, random, passwd)

	resp := [][]byte{
		{byte(2)},
		id,
		hash,
		p.myName}

	return bytes.Join(resp, []byte{}), nil
}
