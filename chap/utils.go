package chap

import (
	"bytes"
	"crypto/md5"
	"encoding/binary"
	"net"
)

func Hash(id, rand, passwd []byte) []byte {
	bs := [][]byte{id, rand, passwd}
	h := md5.Sum(bytes.Join(bs, []byte{}))
	return h[:]
}

func ToBytes(x uint32) []byte {
	bs := make([]byte, 4)
	binary.BigEndian.PutUint32(bs, x)
	return bs
}

func WriteBytes(conn net.Conn, bs []byte) error {
	_, err := conn.Write(append(bs, 0))
	return err
}
