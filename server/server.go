package server

import (
	"chap_lab/chap/host"
	"chap_lab/utils"
	"fmt"
	"net"
)

func Server() {
	chap := host.NewHost("server")
	chap.Database["Alice"] = "123"

	listener, err := startServer()
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	for {
		conn, err := listener.Accept()
		if err != nil {
			continue
		}
		fmt.Println("Accepted new connection!")
		go handle(conn, &chap)
	}
}

func handle(conn net.Conn, chap *host.Host) {
	defer conn.Close()

	name, err := chap.Auth(conn)
	if err != nil {
		fmt.Println("Chap failed:", err.Error())
		return
	}
	fmt.Println("Chap successful")

	fmt.Println("Start chating with", name)
	utils.Chating(conn)
}

func startServer() (*net.TCPListener, error) {
	tcpAddr, err := net.ResolveTCPAddr("tcp4", ":7777")
	if err != nil {
		return nil, err
	}
	listener, err := net.ListenTCP("tcp", tcpAddr)
	if err != nil {
		return nil, err
	}

	return listener, nil
}
