package main

import (
	"chap_lab/client"
	"chap_lab/server"
	"os"
)

func main() {

	switch os.Args[1] {
	case "server":
		server.Server()
	case "client":
		client.Client()
	}

}
