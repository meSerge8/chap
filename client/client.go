package client

import (
	"chap_lab/chap/peer"
	"chap_lab/utils"
	"fmt"
	"net"
)

func Client() {
	conn, err := net.Dial("tcp", "127.0.0.1:7777")
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	defer conn.Close()

	p := peer.NewPeer("Alice")
	p.Database["server"] = "123"

	b, err := p.Auth(conn)
	if err != nil {
		fmt.Println("Chap failed:", err.Error())
		return
	}

	if !b {
		fmt.Println("Chap failed: auth not passed")
		return
	}

	fmt.Println("Start chaing with server")
	utils.Chating(conn)
}
