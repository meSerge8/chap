package utils

import (
	"bufio"
	"fmt"
	"net"
	"os"
)

func Chating(conn net.Conn) {
	actions := make(chan bool)
	defer close(actions)

	go reading(conn, actions)
	go writing(conn, actions)

	fmt.Print(">")
	for ok := range actions {
		if !ok {
			break
		}
		fmt.Print(">")
	}

	fmt.Println("Chating finished")
}

func reading(conn net.Conn, actions chan<- bool) {
	defer func() { actions <- false }()
	r := bufio.NewReader(conn)

	for {
		bs, err := r.ReadBytes('\n')
		if err != nil {
			return
		}
		fmt.Printf("\r%s", string(bs))
		actions <- true
	}

}

func writing(conn net.Conn, actions chan<- bool) {
	defer func() { actions <- false }()

	sc := bufio.NewScanner(os.Stdin)

	for sc.Scan() {
		message := sc.Text()
		if message == "exit" {
			return
		}

		bs := []byte(message + "\n")
		_, err := conn.Write(bs)
		if err != nil {
			return
		}

		actions <- true
	}

}
